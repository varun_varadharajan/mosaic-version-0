from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

# set the default Django settings module for the celery program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'devproject.settings')

app = Celery('mosaic', backend='amqp://')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

if __name__ == '__main__':
    app.start()
