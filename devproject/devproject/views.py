from django.http import HttpResponse
from django.shortcuts import render


def index(request):
  return render(request, 'homepage.html')
  #return render(request, 'base.html')

def login(request):
  return render(request, 'account/login.html')

def account(request):
  return render(request, 'base.html')

def activate(request):
  return render(request, 'account/registrationSuccessful.html')

def instagram(request):
  return render(request, 'instagram.html')

