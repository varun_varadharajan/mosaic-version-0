from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from devproject.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'devproject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^mosaic/', include('mosaic.urls')),
    url(r'^$', 'devproject.views.index', name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^instagram/', 'devproject.views.instagram', name='instagram'),
    url(r'^account/', 'devproject.views.account', name='account'),
    url(r'^login/', 'devproject.views.login', name='login'),
    url(r'^activate/', 'devproject.views.activate', name='activate'),
    url(r'^services/', include('services.urls')),

) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
