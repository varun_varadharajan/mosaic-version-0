from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from rest_framework.authtoken.models import Token
from services.models import Mosaic
import os
import shutil
import imagesearch
import subprocess

#Global variables
IMAGE_CONTAINER_ROOT = os.path.join(os.path.dirname(os.path.realpath(__file__)), "Images")
INPUT_IMAGE_DIR_NAME = "input"
OUTPUT_IMAGE_DIR = "output"
OUTPUT_IMAGE_NAME = "output.jpg"
SEARCH_RESULTS_DIR_NAME = "search"
INPUT_FILE_NAME = "main.jpg"
NUMBER_OF_SEARCH_RESULTS = 300
ALGORITHM_CONTAINER = os.path.join(os.path.dirname(os.path.realpath(__file__)), "algorithm")
EXECUTABLE_NAME = "mosaic.exe"
EXECUTABLE_PATH = os.path.join(ALGORITHM_CONTAINER, EXECUTABLE_NAME)

# Create your views here.
def Home(request):
    if 'HTTP_AUTHORIZATION' in request.META:
        token = request.META['HTTP_AUTHORIZATION'].split()[1]
        user=Token.objects.filter(key=token)[0].user
	mosaics = reversed(Mosaic.objects.filter(user=user))
        return render_to_response('mosaic/dashboard.html', {'username':user, 'mosaics':mosaics}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('http://54.67.25.157/')

def CreateMosaic(request):
    ip = getClientIp(request)
    if request.method == 'POST':
	initializeDirectoryStructure(ip)
	files = request.FILES['imageFile']
	default_storage.save(os.path.join(IMAGE_CONTAINER_ROOT, str(ip), INPUT_IMAGE_DIR_NAME, INPUT_FILE_NAME), ContentFile(files.read()))
        query = request.POST['imageSearch']
        exitCode, numberOfImages = imagesearch.searchAndSave(query, os.path.join(IMAGE_CONTAINER_ROOT, str(ip), SEARCH_RESULTS_DIR_NAME) , 10)
        os.chdir(ALGORITHM_CONTAINER)
	inputImage = os.path.join(IMAGE_CONTAINER_ROOT, str(ip), INPUT_IMAGE_DIR_NAME, INPUT_FILE_NAME)
	searchResultDirectory = os.path.join(IMAGE_CONTAINER_ROOT, str(ip), SEARCH_RESULTS_DIR_NAME)
	outputDirectory = os.path.join(IMAGE_CONTAINER_ROOT, str(ip), OUTPUT_IMAGE_DIR, OUTPUT_IMAGE_NAME)
        #return HttpResponse(EXECUTABLE_PATH+" "+inputImage+" "+searchResultDirectory+" "+str(numberOfImages)+" "+outputDirectory)
	mosaicExe = subprocess.check_call([EXECUTABLE_PATH, inputImage, searchResultDirectory+"/", str(numberOfImages), "/home/ubuntu/emot.com/devproject/static/test.jpg"])
	#output = mosaicExe.communicate()
        #mosaicExe.wait()
	#return render(request, 'mosaic/result.html', {'output' : mosaicExe})
	outputImage = open("/home/ubuntu/emot.com/devproject/static/test.jpg", "r").read()
	shutil.rmtree(os.path.join(IMAGE_CONTAINER_ROOT, str(ip)))
	return HttpResponse(outputImage, content_type="image/jpeg")

#Helper methods
def getClientIp(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def initializeDirectoryStructure(ip):
    pathToStoreImages = os.path.join(IMAGE_CONTAINER_ROOT, str(ip))
    if not os.path.exists(pathToStoreImages):
        os.makedirs(pathToStoreImages)
    pathToStoreInputImage = os.path.join(pathToStoreImages, INPUT_IMAGE_DIR_NAME)
    if not os.path.exists(pathToStoreInputImage):
        os.makedirs(pathToStoreInputImage)
    pathToStoreSearchResults = os.path.join(pathToStoreImages, SEARCH_RESULTS_DIR_NAME)
    if not os.path.exists(pathToStoreSearchResults):
        os.makedirs(pathToStoreSearchResults)
    pathToStoreSearchResults = os.path.join(pathToStoreImages, SEARCH_RESULTS_DIR_NAME)
    pathToOutput = os.path.join(pathToStoreImages, OUTPUT_IMAGE_DIR)
    if not os.path.exists(pathToOutput):
        os.makedirs(pathToOutput)
    

