import os
import urllib
from pprint import pprint
from apiclient.discovery import build

def searchAndSave(query, pathToStoreImages, numberOfResults):
    service=build("customsearch", "v1", developerKey="AIzaSyAl8L3CxZfIfOHC-h56lDqrPee6fw2mMAw")

    if not os.path.exists(pathToStoreImages):
        os.makedirs(pathToStoreImages)
    startIndex = 1
    numberOfImagesDownloaded = 0
    searchFlag = True
    while numberOfResults > 10:
        results=fetch_results(service, query, 10, startIndex)
        numberOfResults = numberOfResults - 10
        startIndex = startIndex + 10
        if not 'items' in results:
            searchFlag= false
            break
        store_images(results, pathToStoreImages, numberOfImagesDownloaded+1)
        numberOfImagesDownloaded += 10
    if searchFlag == True:
        results = fetch_results(service, query, numberOfResults, startIndex)
        if not 'items' in results:
            return 1, numberOfImagesDownloaded
        store_images(results, pathToStoreImages, numberOfImagesDownloaded+1)
        numberOfImagesDownloaded += numberOfResults
        return 0, numberOfImagesDownloaded
    return 1, numberOfImagesDownloaded

def fetch_results(service, query, numberOfResults, startIndex):
    results = service.cse().list(
        q=query,
        cx='016498040335774017160:xlmhth-vq-i',
        searchType='image',
        imgSize='small',
        fileType='jpg',
        num=numberOfResults,
        start=startIndex
        ).execute()

    return results

def store_images(results, pathToImageRepository, imageNameStartIndex):
    for image in results['items']:
        urllib.urlretrieve(image['link'], os.path.join(pathToImageRepository, str(imageNameStartIndex) + ".jpg"))
        imageNameStartIndex += 1
