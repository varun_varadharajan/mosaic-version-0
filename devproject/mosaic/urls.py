from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from mosaic.views import *

urlpatterns = patterns('',
    url(r'^$', 'mosaic.views.Home', name='Home'),
    url(r'result/', 'mosaic.views.CreateMosaic', name='CreateMosaic'),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
