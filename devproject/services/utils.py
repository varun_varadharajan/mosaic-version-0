from django.conf import settings as django_settings
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from rest_framework import response, status
from django.core.files import File
import os, shutil, urllib, requests
from threading import Thread 
from . import settings, constants, imagesearch

def encode_uid(pk):
    try:
        from django.utils.http import urlsafe_base64_encode
        from django.utils.encoding import force_bytes
        return urlsafe_base64_encode(force_bytes(pk)).decode()
    except ImportError:
        from django.utils.http import int_to_base36
        return int_to_base36(pk)

def decode_uid(pk):
    try:
        from django.utils.http import urlsafe_base64_decode
        return urlsafe_base64_decode(pk)
    except ImportError:
        from django.utils.http import base36_to_int
        return base36_to_int(pk)

def send_email(to_email, from_email, context, subject_template_name,plain_body_template_name, html_body_template_name=None):
    subject = loader.render_to_string(subject_template_name, context)
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string(plain_body_template_name, context)
    email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
    if html_body_template_name is not None:
        html_email = loader.render_to_string(html_body_template_name, context)
        email_message.attach_alternative(html_email, 'text/html')
    email_message.send()

def ResizeImage(modelInstance):
    from PIL import Image
    imageFile = Image.open(constants.MEDIA_DIR_NAME+os.path.basename(modelInstance.mosaic.name))
    width, height = imageFile.size
    resizedImage = imageFile.resize((width/3, height/3), Image.ANTIALIAS)
    outputImage = os.path.splitext(os.path.basename(modelInstance.raw_image.name))[0] + "_thumbnail.jpg"
    outputImage = constants.MEDIA_DIR_NAME + outputImage
    resizedImage.save(outputImage)
    outputImageFile = open(outputImage)
    modelInstance.thumbnail = File(outputImageFile)
    modelInstance.save()
    outputImageFile.close()
    os.remove(outputImage)

class ActionViewMixin(object):

    def post(self, request):
        serializer = self.get_serializer(data=request.DATA)
        if serializer.is_valid():
            return self.action(serializer)
        else:
            return response.Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST,
            )


class SendEmailViewMixin(object):

    def send_email(self, to_email, from_email, context):
        send_email(to_email, from_email, context, **self.get_send_email_extras())

    def get_send_email_kwargs(self, user):
        return {
            'from_email': getattr(django_settings, 'DEFAULT_FROM_EMAIL', None),
            'to_email': user.email,
            'context': self.get_email_context(user),
        }

    def get_send_email_extras(self):
        raise NotImplemented

    def get_email_context(self, user):
        token = self.token_generator.make_token(user)
        uid = encode_uid(user.pk)
        return {
            'user': user,
            'domain': settings.get('DOMAIN'),
            'site_name': settings.get('SITE_NAME'),
            'uid': uid,
            'token': token,
            'protocol': 'https' if self.request.is_secure() else 'http',
        }


class GoogleImageSearch(object):
    query = ""
    directoryToSave = ""
    numberOfImages = ""
    user = ""

    def initializeDirectoryStructure(self):
        pathToStoreImages = self.directoryToSave
        if os.path.exists(pathToStoreImages):
            shutil.rmtree(pathToStoreImages)
        os.makedirs(pathToStoreImages)

    def __init__(self, query, user):
        self.query = query
        self.numberOfImages = constants.NUMBER_OF_SEARCH_RESULTS
        self.user = user
        self.directoryToSave = os.path.join(constants.IMAGE_CONTAINER_ROOT, user.username)
        self.initializeDirectoryStructure()

    def cleanup(self):
        if os.path.exists(self.directoryToSave):
            shutil.rmtree(self.directoryToSave)

    def GetFacebookImages(self, imageLinksArray):
        imageNameStartIndex = len([name for name in os.listdir(self.directoryToSave) if os.path.isfile(os.path.join(self.directoryToSave,name))]) + 1
        for imageLink in imageLinksArray:
            urllib.urlretrieve(imageLink, os.path.join(self.directoryToSave, str(imageNameStartIndex) + ".jpg"))
            imageNameStartIndex += 1

    def GetInstagramPhotos(self, instagramUsername):
        userIdQuery = requests.get('https://api.instagram.com/v1/users/search?q='+instagramUsername+'&count=1&client_id='+settings.INSTAGRAM_CLIENT_ID)
        if userIdQuery.json()["meta"]["code"] == 200:
           userId = userIdQuery.json()["data"][0]["id"]
           mediaQuery = requests.get('https://api.instagram.com/v1/users/'+str(userId)+'/media/recent/?client_id='+settings.INSTAGRAM_CLIENT_ID+'&count='+str(constants.INSTAGRAM_PHOTO_COUNT))
           imageLinkArray = []
           if mediaQuery.json()["meta"]["code"] == 200:
               data = mediaQuery.json()["data"]
               for image in data:
                   if image["type"] == 'image':
                       imageLinkArray.append(image["images"]["low_resolution"]["url"])
               self.GetFacebookImages(imageLinkArray)
           
    def ImageSearch(self):
        exitCode, numberOfImages = imagesearch.searchAndSave(self.query, self.directoryToSave, constants.NUMBER_OF_SEARCH_RESULTS)
