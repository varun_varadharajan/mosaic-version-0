from django.contrib.auth import get_user_model, login
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.models import User
from django.core.files import File
from django.views.decorators.csrf import csrf_exempt
from celery.result import AsyncResult
from celery.app.task import Task
from rest_framework import generics, permissions, status, response, renderers
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.authentication import get_authorization_header
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.decorators import api_view, permission_classes
from rest_framework.authtoken.serializers import AuthTokenSerializer
from social.apps.django_app.utils import strategy, psa, load_strategy, load_backend
from devproject import settings as projsettings
from . import serializers, settings, utils, viewsets, constants, tasks
from models import Mosaic
from django.core.urlresolvers import reverse
import json,os
import subprocess

User = get_user_model()

@strategy()
def auth_by_token(request, backend):
    uri = ''
    strategy = load_strategy(request)
    backend = load_backend(strategy, backend, uri)
    access_token = request.DATA.get('access_token')
    user = backend.do_auth(access_token)
    return user

class RegistrationView(utils.SendEmailViewMixin, generics.CreateAPIView):
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def get_serializer_class(self):
        if settings.get('LOGIN_AFTER_REGISTRATION'):
            return serializers.UserRegistrationWithAuthTokenSerializer
        return serializers.UserRegistrationSerializer

    def post_save(self, obj, created=False):
        if settings.get('LOGIN_AFTER_REGISTRATION'):
            Token.objects.get_or_create(user=obj)
        if settings.get('SEND_ACTIVATION_EMAIL'):
            self.send_email(**self.get_send_email_kwargs(obj))

    def get_send_email_extras(self):
        return {
            'subject_template_name': 'activation_email_subject.txt',
            'plain_body_template_name': 'activation_email_body.txt',
        }

    def get_email_context(self, user):
        context = super(RegistrationView, self).get_email_context(user)
        context['url'] = settings.get('ACTIVATION_URL').format(**context)
        return context

class LoginView(utils.ActionViewMixin, generics.GenericAPIView):
    serializer_class = serializers.UserLoginSerializer
    permission_classes = (
        permissions.AllowAny,
    )

    def action(self, serializer):
        token, _ = Token.objects.get_or_create(user=serializer.object)
        return Response(
            data=serializers.TokenSerializer(token).data,
            status=status.HTTP_200_OK,
        )


class LogoutView(generics.GenericAPIView):
    serializer_class = serializers.UserLoginSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request):
        Token.objects.filter(user=request.user).delete()
        return response.Response(status=status.HTTP_200_OK)

class GetMosaic(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.MosaicSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )
    def get(self, request, *args, **kwargs):
        mosaics = Mosaic.objects.filter(user=request.user)
        serializer = serializers.MosaicSerializer(mosaics, many=True)
        return Response(serializer.data)

class CreateMosaic(generics.GenericAPIView):
    serializer_class=serializers.MosaicSerializer
    parser_classes = (
        MultiPartParser,
        FormParser,
    )
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request, *args, **kwargs):
        image = request.FILES['image_file']
        metadata = request.POST['metadata']
        owner = request.user
        instance = Mosaic(user=owner, raw_image=image, metadata=metadata)
        instance.save()
        mosaic = tasks.CreateMosaic.delay(instance.id)
        return Response(data=mosaic.id, status=status.HTTP_200_OK)

class GetMosaicStatus(generics.GenericAPIView):
    serializer_class = serializers.MosaicSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request, *args, **kwargs):
        taskid = request.DATA.get('task_id')
        task = AsyncResult(str(taskid))
        if not task:
            return Response(data="Invalid task", status=status.HTTP_400_BAD_REQUEST)
        if task.ready():
            if task.successful():
                return Response(data="Mosaic Successfully Created", status=status.HTTP_200_OK)
            else:
                return Response(data="Mosaic Creation Failed", status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data=json.dumps(task.result), status=status.HTTP_200_OK)

class UpdateMosaicStatus(generics.GenericAPIView):
    serializer_class = serializers.MosaicSerializer
    permission_classes = (
        permissions.AllowAny,
    )

    def post(self, request, *args, **kwargs):
        task_id=request.DATA.get('task_id')
        secret_key = request.DATA.get('secret_key')
        completion_status = request.DATA.get('completion_status')
        if secret_key == projsettings.MOSAIC_SECRET_KEY:
            try:
                task = AsyncResult(str(task_id))
                result = task.result
                result['completion_status'] = completion_status
                Task().update_state(task.id, state="PROGRESS", meta=result)
                return Response(status=status.HTTP_200_OK)
            except Exception:
                return Response(status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class PasswordResetView(utils.ActionViewMixin, utils.SendEmailViewMixin, generics.GenericAPIView):
    serializer_class = serializers.PasswordResetSerializer
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def action(self, serializer):
        for user in self.get_users(serializer.data['email']):
            self.send_email(**self.get_send_email_kwargs(user))
        return response.Response(status=status.HTTP_200_OK)

    def get_users(self, email):
        active_users = User._default_manager.filter(
            email__iexact=email,
            is_active=True,
        )
        return (u for u in active_users if u.has_usable_password())

    def get_send_email_extras(self):
        return {
            'subject_template_name': 'password_reset_email_subject.txt',
            'plain_body_template_name': 'password_reset_email_body.txt',
        }

    def get_email_context(self, user):
        context = super(PasswordResetView, self).get_email_context(user)
        context['url'] = settings.get('PASSWORD_RESET_CONFIRM_URL').format(**context)
        return context

class SetPasswordView(utils.ActionViewMixin, generics.GenericAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def get_serializer_class(self):
        if settings.get('SET_PASSWORD_RETYPE'):
            return serializers.SetPasswordRetypeSerializer
        return serializers.SetPasswordSerializer

    def action(self, serializer):
        self.request.user.set_password(serializer.data['new_password'])
        self.request.user.save()
        return response.Response(status=status.HTTP_200_OK)

class PasswordResetConfirmView(utils.ActionViewMixin, generics.GenericAPIView):
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def get_serializer_class(self):
        if settings.get('PASSWORD_RESET_CONFIRM_RETYPE'):
            return serializers.PasswordResetConfirmRetypeSerializer
        return serializers.PasswordResetConfirmSerializer

    def action(self, serializer):
        serializer.user.set_password(serializer.data['new_password'])
        serializer.user.save()
        return response.Response(status=status.HTTP_200_OK)


class ActivationView(utils.ActionViewMixin, generics.GenericAPIView):
    serializer_class = serializers.UidAndTokenSerializer
    permission_classes = (
        permissions.AllowAny,
    )
    token_generator = default_token_generator

    def action(self, serializer):
        serializer.user.is_active = True
        serializer.user.save()
        if settings.get('LOGIN_AFTER_ACTIVATION'):
            token, _ = Token.objects.get_or_create(user=serializer.user)
            data = serializers.TokenSerializer(token).data
        else:
            data = {}
        return Response(data=data, status=status.HTTP_200_OK)


class SetUsernameView(utils.ActionViewMixin, generics.GenericAPIView):
    serializer_class = serializers.SetUsernameSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def get_serializer_class(self):
        if settings.get('SET_USERNAME_RETYPE'):
            return serializers.SetUsernameRetypeSerializer
        return serializers.SetUsernameSerializer

    def action(self, serializer):
        setattr(self.request.user, User.USERNAME_FIELD, serializer.data['new_' + User.USERNAME_FIELD])
        self.request.user.save()
        return response.Response(status=status.HTTP_200_OK)


class UserView(generics.RetrieveUpdateAPIView):
    model = User
    serializer_class = serializers.UserSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )
    renderer_classes = (
        renderers.JSONRenderer,
    )

    def get_object(self, *args, **kwargs):
        return self.request.user

class SocialRegister(generics.GenericAPIView):
    serializer_class = AuthTokenSerializer
    permission_classes = (
        permissions.AllowAny,
    )
    model = Token

    def post(self, request):
        auth_token = request.DATA.get('access_token', None)
        backend = request.DATA.get('backend', None)
        if auth_token and backend:
            try:
                user = auth_by_token(request, backend)
            except Exception as err:
                return Response("Error: " + str(err), status=status.HTTP_400_BAD_REQUEST)
            if user and user.is_active:
                token, created = Token.objects.get_or_create(user=user)
                return Response({'auth_token': token.key}, status=status.HTTP_200_OK)
            else:
                return Response("Bad Credentials", status=status.HTTP_403_FORBIDDEN)
        else:
            return Response("Bad request", status=status.HTTP_400_BAD_REQUEST)
