from django.core.exceptions import ImproperlyConfigured

def get(key):
    from django.conf import settings
    defaults = {
        'LOGIN_AFTER_REGISTRATION': False,
        'LOGIN_AFTER_ACTIVATION': False,
        'SEND_ACTIVATION_EMAIL': False,
        'SET_PASSWORD_RETYPE': False,
        'SET_USERNAME_RETYPE': False,
        'PASSWORD_RESET_CONFIRM_RETYPE': False,
    }
    defaults.update(getattr(settings, 'SERVICES', {}))
    try:
        return defaults[key]
    except KeyError:
        raise ImproperlyConfigured('Missing settings: Emotiful Services[\'{}\']'.format(key))


GOOGLE_APP_NAME = "customsearch"
GOOGLE_APP_VERSION = "v1"
GOOGLE_DEVELOPER_KEY = "AIzaSyDMg7JuF9BpQ5i3qa9cZzbOwWG_aRqrRUk"
GOOGLE_CUSTOM_SEARCH_ENGINE_ID = "006114850748958098073:be94mhljatc"

INSTAGRAM_CLIENT_ID = "1bd2096350b2424f86e3296bfe9524a8"
