import os
import urllib
from pprint import pprint
from apiclient.discovery import build
from PIL import Image
from . import settings

def searchAndSave(query, pathToStoreImages, numberOfResults):
    service=build(settings.GOOGLE_APP_NAME, settings.GOOGLE_APP_VERSION, developerKey=settings.GOOGLE_DEVELOPER_KEY)

    if not os.path.exists(pathToStoreImages):
        os.makedirs(pathToStoreImages)
    startIndex = 1
    numberOfImagesDownloaded = 0
    searchFlag = True
    while numberOfResults > 10:
        results=fetch_results(service, query, 10, startIndex)
        numberOfResults = numberOfResults - 10
        startIndex = startIndex + 10
        if not 'items' in results:
            searchFlag= false
            break
        store_images(results, pathToStoreImages)
        numberOfImagesDownloaded += 10
    if searchFlag == True:
        results = fetch_results(service, query, numberOfResults, startIndex)
        if not 'items' in results:
            return 1, numberOfImagesDownloaded
        store_images(results, pathToStoreImages)
        numberOfImagesDownloaded += numberOfResults
        return 0, numberOfImagesDownloaded
    return 1, numberOfImagesDownloaded

def fetch_results(service, query, numberOfResults, startIndex):
    results = service.cse().list(
        q=query,
        cx=settings.GOOGLE_CUSTOM_SEARCH_ENGINE_ID,
        searchType='image',
        imgSize='small',
        fileType='jpg',
        num=numberOfResults,
        start=startIndex
        ).execute()

    return results

def store_images(results, pathToImageRepository):
    imageNameStartIndex = len([name for name in os.listdir(pathToImageRepository) if os.path.isfile(os.path.join(pathToImageRepository,name))]) + 1
    for image in results['items']:
        urllib.urlretrieve(image['link'], os.path.join(pathToImageRepository, str(imageNameStartIndex) + ".jpg"))
        try:
            i=Image.open(os.path.join(pathToImageRepository, str(imageNameStartIndex) + ".jpg"))
            imageNameStartIndex += 1
        except Exception as err:
            print err
            os.remove(os.path.join(pathToImageRepository, str(imageNameStartIndex) + ".jpg"))


