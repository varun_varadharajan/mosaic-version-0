from rest_framework import mixins, generics

class CreateListModelViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, generics.GenericAPIView):
    pass
