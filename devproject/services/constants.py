from django.utils.translation import ugettext_lazy as _
import os

INVALID_CREDENTIALS_ERROR = _('Unable to login with the credentials provided.')
DISABLE_ACCOUNT_ERROR = _('User account is disabled.')
INVALID_TOKEN_ERROR = _('Invalid token for the given user.')
PASSWORD_MISMATCH_ERROR = _('The two password fields didn\'t match.')
USERNAME_MISMATCH_ERROR = _('The two  {} fields didn\'t match.')
INVALID_PASSWORD_ERROR = _('Invalid password.')

ALGORITHM_CONTAINER = os.path.join(os.path.dirname(os.path.realpath(__file__)), "algorithm")
EXECUTABLE_NAME = "mosaic.exe"
EXECUTABLE_PATH = os.path.join(ALGORITHM_CONTAINER, EXECUTABLE_NAME)
IMAGE_CONTAINER_ROOT = os.path.join(os.path.dirname(os.path.realpath(__file__)), "GoogleSearchImages")
MEDIA_DIR_NAME = "/home/ubuntu/emot.com/devproject/media/"
NUMBER_OF_SEARCH_RESULTS = 30

RESIZED_IMAGE_WIDTH = 1024
RESIZED_IMAGE_HEIGHT = 768

INSTAGRAM_PHOTO_COUNT = 100
