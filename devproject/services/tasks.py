from __future__ import absolute_import

from celery import shared_task, current_task, task
from services import utils, models, constants
from django.core.files import File
import json, os, subprocess, time

@shared_task
def add(x, y):
    current_task.update_state(state='PROGRESS', meta={'task_status':'Completed Downloading Themes'})
    print current_task.request.id
    time.sleep(100)
    return x + y


@shared_task
def mul(x, y):
    return x * y


@shared_task
def xsum(numbers):
    return sum(numbers)

@task()
def CreateMosaic(mosaicId):
    modelInstance = models.Mosaic.objects.get(id=mosaicId)
    metadata = json.loads(modelInstance.metadata)
    imageSearch = utils.GoogleImageSearch(query=metadata['query'], user=modelInstance.user)
    if metadata['query'] != "undefined":
        current_task.update_state(state='PROGRESS', meta={'task_status':'Started Downloading Themes', 'completion_status':'5'})
        imageSearch.ImageSearch()
        current_task.update_state(state='PROGRESS', meta={'task_status':'Completed Downloading Themes', 'completion_status':'5'})
    if metadata['facebook']:
        current_task.update_state(state='PROGRESS', meta={'task_status':'Started Downloading Social Photos', 'completion_status':'5'})
        imageSearch.GetFacebookImages(imageLinksArray=metadata['facebook'])
        current_task.update_state(state='PROGRESS', meta={'task_status':'Completed Downloading Social Photos', 'completion_status':'5'})
    if metadata['instagram']:
        current_task.update_state(state='PROGRESS', meta={'task_status':'Started Downloading Social Photos', 'completion_status':'6'})
        imageSearch.GetInstagramPhotos(instagramUsername=metadata['instagram'])
        current_task.update_state(state='PROGRESS', meta={'task_status':'Completed Downloading Social Photos', 'completion_status':'6'})

    imageDirectory = os.path.join(constants.IMAGE_CONTAINER_ROOT, modelInstance.user.username)
    numberOfImages = len([name for name in os.listdir(str(imageDirectory)) if os.path.isfile(os.path.join(imageDirectory,name))])
    outputImage = os.path.splitext(os.path.basename(modelInstance.raw_image.name))[0] + "Mosaic.jpg"
    outputImage = constants.MEDIA_DIR_NAME + outputImage
    current_task.update_state(state='PROGRESS', meta={'task_status':'Creating mosaic', 'completion_status':'5'})
    mosaicExe = subprocess.check_call([constants.EXECUTABLE_PATH, modelInstance.raw_image.path, str(os.path.join(constants.IMAGE_CONTAINER_ROOT, modelInstance.user.username))+"/", str(numberOfImages), outputImage, current_task.request.id])
    outputImageFile = open(outputImage)
    modelInstance.mosaic = File(outputImageFile)
    modelInstance.save()
    outputImageFile.close()
    os.remove(outputImage)
    utils.ResizeImage(modelInstance)
    imageSearch.cleanup()

