from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Mosaic(models.Model):
    user = models.ForeignKey(User)
    mosaic = models.FileField(blank=False)
    raw_image = models.FileField(blank=False)
    metadata = models.TextField()
    thumbnail = models.FileField(blank=True, null=True, default=None)
