window.fbAsyncInit = function() {
    FB.init({
        appId      : '812543085455112',
        status     : true,
	version    : 'v2.2',
	oauth      : true,
        xfbml      : true
    });
};
    
(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function shareMosaicOnFacebook(imageUrl) {
    FB.ui(
        {
            method: 'feed',
            name: 'Mosaic by Emotiful',
	    description: 'A mosac created by Emotiful, Inc',
            picture: imageUrl,
            link: imageUrl,
	    message: 'Your social photos shuffled: sexy, stylish, emotiful'
	});
}

function getFacebookUserImages(fn) {
    FB.getLoginStatus(function(loginStatus) {
      if(loginStatus.status !== 'connected') {
        FB.login(
		function(response){
		  if(response.authResponse) {
		    FB.api(
		      "/me/photos",
      		      {limit: 100},
      		      function (response) {
        		var facebookPhotos = new Array()
        		if (response && !response.error) {
          		  for(var i=0; i<response.data.length; ++i) {
              		    images = response.data[i].images;
              		    facebookPhotos[i] = '"' + images[images.length-1].source +'"';
          		  }
        		}
       			fn(facebookPhotos);
      		      }
      		    );
		  }
		}, {scope:'user_photos'}, {perms:'user_photos'})
      }
      FB.api(
      "/me/photos",
      {limit: 100},
      function (response) {
        var facebookPhotos = new Array()
        if (response && !response.error) {
          for(var i=0; i<response.data.length; ++i) {
 	      images = response.data[i].images;
              facebookPhotos[i] = '"' + images[images.length-1].source +'"';
          }
        }
        fn(facebookPhotos);
      }
      );
  });
}
