appServices.factory('AuthenticationService', function() {
	var auth = {
		isAuthenticated: false
	}
	return auth;
});

appServices.factory('fileUpload', ['$http', '$window', function ($http, $window) {
    function getstatus(data) {
        //console.log(data)
        taskId = data;
        $http({
                method: 'POST',
                url: 'http://54.67.25.157/services/getstatus',
                headers: {'Content-Type': 'application/x-www-form-urlencoded','Authorization':'Token '+ $window.sessionStorage.token},
                data: "task_id="+taskId
        })
        .success(function(data, status, headers, config){
               console.log(data);
		 if(data.indexOf("Failed to create a mosaic") != -1) {
		   alert("Failed to create a mosaic. Please try again later").
		   window.location.reload();
		 }
                 if(data.indexOf("Mosaic Successfully Created") != -1) {
		    progressJs("#progressBar").end();
                    window.location.reload();
                 }
                 if(data &&  data !== "null") {
                   jsObj = JSON.parse(data);
		   try {
                     if(jsObj && jsObj['completion_status']){
		       progressJs("#progressBar").set(parseInt(jsObj.completion_status));
                       setTimeout(function(){ getstatus(taskId) }, 5000);
                     }
		   }
		   catch(err) {
                     setTimeout(function(){ getstatus(taskId) }, 5000);
                   }
                 }
                 else {
                   //document.getElementById("statusMessage").inerHTML = "Starting Mosaic Creation";
                   setTimeout(function(){ getstatus(taskId) }, 5000);
                 }
        })
        .error(function(){
            console.log("Failed to create a mosaic");
        });
    }
    return {
            uploadFileToUrl: function(file, metadata, uploadUrl){
            var fd = new FormData();
            fd.append('image_file', file);
            fd.append('metadata', metadata)
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined, 'Authorization':'Token '+ $window.sessionStorage.token}
            })
            .success(function(data, status, headers, config){
		document.getElementById("progressBar").style.display = "block";
		progressJs("#progressBar").setOptions({ 'theme': 'blueOverlayRadiusWithPercentBar', 'overlayMode': true }).start().set(2);
                getstatus(data);
            })
            .error(function(){
              alert("Error uploading photo. Please try again.");
            });
        }
    }
}]);

appServices.factory('UserService', function($http, $window, $q, $rootScope) {

        var resolve = function(errval, retval, deferred) {
	    $rootScope.$apply(function() {
 	        if(errval) {
		    deferred.reject(errval);
	        }
		else {
		    retval.connected = true;
		    deferred.resolve(retval);
		}
	    });
	}

        var _login = function() {
	    var deferred = $q.defer();
	    FB.getLoginStatus(function(response){
		if(response.status === 'connected') {
		    deferred.resolve(response);
		}
		else {
		    FB.login(function(response) {
			if(response.authResponse) {
			    resolve(null, response, deferred);
			}
			else {
			   resolve(response.error, null, deferred);
			}
		    },
		    {
		        scope: 'email, user_photos'
		    },
		    {
  		        perms: 'email, user_photos'
		    });
		}
	    });
	    return deferred.promise;
        }

        return {
                logIn: function(username, password) {
                        return $http({
                                method: 'POST',
                                url: 'http://54.67.25.157/services/login',
                                headers: {'Content-Type': 'application/x-www-form-urlencoded' },
                                data: "username="+username+"&password="+password
                        	});
			},
                logout: function() {
                        return $http({
                                method: 'POST',
                                url: 'http://54.67.25.157/services/logout',
                                headers: {'Content-Type': 'application/x-www-form-urlencoded' },
                                data: $window.sessionStorage.token
                		});
			},
                facebookLogin: _login
	};
});       

appServices.factory('TokenInterceptor', function ($q, $window, $location, AuthenticationService) {
	return {
		request: function (config) {
		config.headers = config.headers || {};
		if ($window.sessionStorage.token) {
			config.headers.Authorization = 'Token ' + $window.sessionStorage.token;
		}
		return config;
	},
	requestError: function(rejection) {
		return $q.reject(rejection);
	},
	/* Set Authentication.isAuthenticated to true if 200 received */
	response: function (response) {
		if (response != null && response.status == 200 && $window.sessionStorage.token && !AuthenticationService.isAuthenticated) {
			AuthenticationService.isAuthenticated = true;
		}
		return response || $q.when(response);
	},
	/* Revoke client authentication if 401 is received */
	responseError: function(rejection) {
	if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || AuthenticationService.isAuthenticated)) {
		delete $window.sessionStorage.token;
		AuthenticationService.isAuthenticated = false;
		$location.path("/");
	}
	return $q.reject(rejection);
	}
    };
});
