appDirectives.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

appDirectives.directive('tooltip', function(){
  return {
    restrict: 'A',
    link: function(scope,element) {
        $(element).tooltip();
        }
    };
});

appDirectives.directive('fbshare',function(){
  return {
    restrict:'A',
    link: function(scope,element) {
        $(element).on('slide', function(event, index, slide) {
            var fbShare = element.find('[tooltip=""]');
            var link = $(slide.children[0]).attr("src");
            fbShare.unbind('click').click(function() {
                $(element).data('gallery').close();
                shareMosaicOnFacebook(link);
                })
            });
        }
    };
});

appDirectives.directive('gallery', function() { 
  return {
    restrict: 'A',
    link: function(scope,element) { 
      console.log("Justifying Gallery")
      $(element).justifiedGallery({
        rowHeight : 400,
        lastRow : 'nojustify',
        margins : 1
      });
    }
  };
});

appDirectives.directive('menu', function() {
 return {
   restrict: 'A',
   link: function(scope,element) {
     $(element).click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
     });
   }
 }
});
