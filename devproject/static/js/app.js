var app = angular.module('mosaic', ['ngRoute', 'appServices', 'appControllers', 'appDirectives']);
var appServices = angular.module('appServices', []);
var appControllers = angular.module('appControllers', []);
var appDirectives = angular.module('appDirectives', []);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider.
		when('/', {
			templateUrl: 'http://54.67.25.157/login',
			controller: 'AuthenticationController'
		}).
		when('/activate', {
			templateUrl: 'http://54.67.25.157/activate'
		}).
		when('/mosaic', {
			templateUrl: 'http://54.67.25.157/mosaic',
			access: { requiredAuthentication: true }
		}).
                /*when('/', {
			templateUrl: 'http://54.67.25.157/hp'
		}).*/
		otherwise({
			redirectTo: '/'
		});
                //$locationProvider.html5Mode(true);
                //$locationProvider.hashPrefix('!');
	}]);


app.config(function($httpProvider) {
	$httpProvider.interceptors.push('TokenInterceptor');
});

app.run(function($rootScope, $location, $window, AuthenticationService) {
	$rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
	    if(nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication 
		&& !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {
			$location.path("/");
	    }
	});
});
