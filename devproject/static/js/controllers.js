
appControllers.controller("AuthenticationController",['$scope', '$http', '$location', '$window', 'UserService', 'AuthenticationService', 'fileUpload',
    function($scope, $http, $location, $window, UserService, AuthenticationService, fileUpload) {
        function getLocation() {
                return $location;
        }
	$scope.login = function() {
		var username = $scope.username;
		var password = $scope.password;
		if(username !== undefined && password !== undefined) {
			UserService.logIn(username, password).success(function(data) {
			AuthenticationService.isLogged = true;
			$window.sessionStorage.token = data.auth_token;
			getLocation().path('/mosaic').replace();
			}).error(function(status, data) {
				console.log(status);
				console.log(data);
			});
		}
	}
        $scope.login_fb = function(){
            UserService.facebookLogin().then(function(response){
                //Contact Backend
                console.log("Facebook says: " + response)
	 	var reqObj = "access_token="+response.authResponse.accessToken+"&backend=facebook"
		$http({
                        method: 'POST',
                        url: 'http://54.67.25.157/services/sociallogin',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded' },
                        data: reqObj
                }).
                success(function(data, status, headers, config) {
                        console.log("Emotiful Says:" + data)
			AuthenticationService.isLogged = true;
 			$window.sessionStorage.token = data.auth_token;
			getLocation().path('/mosaic').replace();
                });
            });
        }
	$scope.logout = function() {
		if(AuthenticationService.isAuthenticated) {
			UserService.logout().success(function(data) {
			AuthenticationService.isAuthenticated = false;
			delete $window.sessionStorage.token;
			getLocation().path("/").replace();
			window.location.reload();
			}).error(function(status, data) {
				console.log(status);
				console.log(error);
			});                      
		}
	}
        $scope.register = function($location) {
                var username = $scope.registerUsername;
                var password = $scope.registerPassword;
                var email = $scope.registerEmail;
                $http({
                        method: 'POST',
                        url: 'http://54.67.25.157/services/register',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded' },
                        data: "username="+username+"&password="+password+"&email="+email
                }).
                success(function(data, status, headers, config) {
                        var location = getLocation();
                        location.path('/activate').replace();
                });
        }
        
}]);

appControllers.controller("UploadController", ['$scope', 'fileUpload', function($scope, fileUpload){
 
    function createRequestObject(googleImageSearchQuery, facebookImages, instagramUsername) {
        var metadata = '{'
			+'"query":"' + googleImageSearchQuery + '",'
                        +'"instagram":"' + instagramUsername +'",'
			+'"facebook":[' + facebookImages + ']'
			+'}';
        return metadata;    
    }   
    $scope.upload = function() {
        var file = $scope.imageFile;
        var uploadUrl = "http://54.67.25.157/services/createmosaic";
        var instagramUsername = ""
        if($scope.instagramUsername)
          instagramUsername = $scope.instagramUsername
        if($scope.facebookPhotos) {
            getFacebookUserImages(function(photoArray) { 
                metadata = createRequestObject($scope.imageSearchQuery, photoArray, instagramUsername);
		fileUpload.uploadFileToUrl(file, metadata, uploadUrl);
            });
        }
        else {
            metadata = createRequestObject($scope.imageSearchQuery, "", isntagramUsername);
            fileUpload.uploadFileToUrl(file, metadata, uploadUrl);
	}
    };
    
}]);
